lxd:
  lxd:
    run_init: True

  python:
    # Currently pylxd version 2 is required for the lxd module to work.
    use_pip: True

lxd:
  containers:
    local:
      web75:
        running: True
        source: ubuntu-base
