install_network_packages:
  pkg.installed:
    - pkgs:
      - wget
      - unzip
      - htop
      - apache2
